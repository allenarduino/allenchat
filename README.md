# Allenchat

- Allenchat is a simple social chatting application.
- It implements the long-polling concept
- A user of this app can edit his or her profile,view users or chat users
- The back end is powered by Node and Mysql database.
- The front end is powered by React Native expo.
- This application is a REST API so you can build a mobile app or desktop app in future to consume the same data from the backend.
- It uses JWT method for authentication and authorization instead of session.
- It uses JWT because it's a REST API.
- Create a database according to the schema design in the Database directory
- cd into Frontend directory and run npm start.

# Coding is fun :)
